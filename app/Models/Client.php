<?php
/**
 * Created by PhpStorm.
 * User: Islam
 * Date: 7/22/2018
 * Time: 7:20 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    public function sessions()
    {
        return $this->hasMany(Session::class);
    }
}