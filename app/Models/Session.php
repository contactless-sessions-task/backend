<?php
/**
 * Created by PhpStorm.
 * User: Islam
 * Date: 7/22/2018
 * Time: 7:20 PM
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed date
 */
class Session extends Model
{
    protected $dates = ['date'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }


}