<?php
/**
 * Created by PhpStorm.
 * User: Islam
 * Date: 7/22/2018
 * Time: 7:20 PM
 */

namespace App\Resources;


use Illuminate\Http\Resources\Json\Resource;

class PhoneNumberResource extends Resource
{
    public function toArray($request)
    {
        return [
            'phone_number' => (string)$this->phone_number
        ];
    }
}