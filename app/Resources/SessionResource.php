<?php
/**
 * Created by PhpStorm.
 * User: Islam
 * Date: 7/22/2018
 * Time: 7:20 PM
 */

namespace App\Resources;


use Illuminate\Http\Resources\Json\Resource;

class SessionResource extends Resource
{
    public function toArray($request)
    {
        return [
            'title' => $this->formatTitle(),
            'date' => $this->date->format('d M'),
            'available_tickets' => $this->available_tickets,
            'description' => $this->description
        ];
    }

    public function formatTitle()
    {
        if ($this->date->isCurrentDay()) {
            return 'Today';
        }
        if ($this->date->isTomorrow()) {
            return 'Tomorrow';
        }
        return $this->date->englishDayOfWeek;
    }
}