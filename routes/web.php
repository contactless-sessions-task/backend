<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Resources\PhoneNumberResource;

$router->get('/', function () use ($router) {
    return '1.0';
});
$router->get('/phone', function () {
    return new PhoneNumberResource(\App\Models\Client::first());
});
$router->get('/logo', function () {
    return new \App\Resources\LogoResource(\App\Models\Client::first());
});
$router->get('/sessions', function () {
    return \App\Resources\SessionResource::collection(\App\Models\Client::first()->sessions);
});
